package info.sidespace.meetro.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by koichi-wada on 2014/11/07.
 */
public class PostData {
    public int mId;
    public String mCategory;
    public String mPostData;
    public int mUserId;
    public String mTimestamp;
    public String mUcode;
    public String mLocUcode;
    public double mLat;
    public double mLng;
    public String mStationNameEn;
    public String mStationNameJp;
    public int mLikeCount;
    public int mLikeUserCount;
    public int mParentPostId;
    public int mReplyCount;
    public int[] mReplyPostIdList;

    public PostData(JSONObject o) throws JSONException {
        mId = o.getInt("post_id");
        mCategory = o.getString("category").trim();
        mPostData = o.getString("post_data").trim();
        mUserId = o.getInt("user_id");
        mTimestamp = o.getString("timestamp").trim();
        mUcode = o.getString("ucode").trim();
        mLocUcode = o.getString("loc_ucode").trim();
        mLat = o.getDouble("lat");
        mLng = o.getDouble("lng");
        mStationNameEn = o.getString("name_en").trim();
        mStationNameJp = o.getString("name_jp").trim();

        // like info
        JSONObject likeInfo = o.getJSONObject("like_info");
        mLikeCount = likeInfo.getInt("count");
        mLikeUserCount = likeInfo.getInt("user_liked");

        // reply info
        mParentPostId = o.getInt("parent_post_id");

        JSONObject replyInfo = o.getJSONObject("reply_info");
        mReplyCount = replyInfo.getInt("count");


        JSONArray postIds = replyInfo.getJSONArray("post_ids");

        int[] replyPostIdList = new int[postIds.length()];
        for (int i = 0; i < postIds.length(); ++i) {
            replyPostIdList[i] = postIds.getInt(i);
        }

        mReplyPostIdList = replyPostIdList;
    }

    public boolean isLiked() {
        return (mLikeUserCount > 0);
    }

    public void like() {
        if (!isLiked()) {
            ++mLikeCount;
            ++mLikeUserCount;
        }
    }

    public void unlike() {
        if (isLiked()) {
            --mLikeCount;
            --mLikeUserCount;
        }
    }


}
