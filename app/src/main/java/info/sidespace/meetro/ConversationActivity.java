package info.sidespace.meetro;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import info.sidespace.meetro.api.NearbyStation;
import info.sidespace.meetro.api.Post;
import info.sidespace.meetro.db.DbHelper;
import info.sidespace.meetro.model.PostData;

public class ConversationActivity extends PostListActivityBase {
    protected int mPostId;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView mListView;
    private JsonHttpResponseHandler mPostRefreshHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        Intent intent = getIntent();
        mPostId = intent.getIntExtra("postId", 0);
        mUserId = intent.getIntExtra("userId", 0);
        mUserName = intent.getStringExtra("userName");
        mUcode = intent.getStringExtra("ucode");
        mStationName = intent.getStringExtra("stationName");

        Log.d("POSTID", Integer.valueOf(mPostId).toString());

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.conversation);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.blue, R.color.green, R.color.orange, R.color.red);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh();
                    }
                }, 500);
            }
        });

        // ListView
        mListView = (ListView) this.findViewById(R.id.postList);
        mListView.setAdapter(new PostArrayAdapter(this, false, new LinkedList<PostData>()));


        mPostRefreshHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                LinkedList<PostData> postList = new LinkedList<PostData>();

                try {
                    int pos = -1;
                    int parentPostId = 0;
                    for (int i = 0; i < response.length(); ++i) {
                        PostData postData = new PostData(response.getJSONObject(i));
                        Log.d("**POSTID", Integer.valueOf(postData.mId).toString());
                        if (postData.mId == mPostId) {
                            pos = i;
                            parentPostId = postData.mParentPostId;
                            postList.add(postData);
                            break;
                        }
                    }

                    if (pos > -1) {
                        Set<Integer> parentPostIdSet = new HashSet<Integer>();
                        parentPostIdSet.add(Integer.valueOf(mPostId));

                        // Adding posts replied to the focused post.
                        for (int i = pos - 1; i > -1; --i) {
                            PostData postData = new PostData(response.getJSONObject(i));
                            if (parentPostIdSet.contains(Integer.valueOf(postData.mParentPostId))) {
                                postList.addFirst(postData);
                                parentPostIdSet.add(Integer.valueOf(postData.mId));
                            }
                        }

                        // Adding ancesters of the focused post.
                        for (int i = pos + 1; i < response.length(); ++i) {
                            PostData postData = new PostData(response.getJSONObject(i));
                            if (parentPostId == postData.mId) {
                                postList.add(postData);
                                parentPostId = postData.mParentPostId;
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.d("Exception", e.toString());
                }
                PostArrayAdapter adapter = (PostArrayAdapter) mListView.getAdapter();
                adapter.clear();
                adapter.addAll(postList);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onFinish() {
                super.onFinish();

                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        };
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

    @Override
    protected void onResume() {
        super.onResume();

        refresh();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void refresh() {
        Post.getByUcode(mUserId, Post.TYPE_RECENT, mUcode, 0, mPostRefreshHandler);
    }
}
