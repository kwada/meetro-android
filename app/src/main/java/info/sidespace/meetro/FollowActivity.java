package info.sidespace.meetro;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import info.sidespace.meetro.api.User;
import info.sidespace.meetro.db.DbHelper;

/**
 * Created by Pieris_rapae on 2014/11/11.
 */
public class FollowActivity extends Activity {

    // 传递数据的 key
    public static final String DATA_KEY_NAME = "flo_name";
    public static final String DATA_KEY_HEAD_URL = "flo_headurl";
    public static final String DATA_KEY_FOLLOWING_NUMBER = "flo_following_number";
    public static final String DATA_KEY_FOLLOWER_NUMBER = "flo_follower_number";
    public static final String DATA_KEY_FAV_STATION = "flo_station";
    public static final String DATA_KEY_FAV_LINE = "flo_line";
    public static final String DATA_KEY_HAS_FOLLOW = "flo_has_followed";

    private TextView name, following_number, follower_number, favourite_station, favourite_line = null;

    private TextView follow_button = null;


    private int mUserId, mMyUserId;
    private String mUserName;

    // DB
    private DbHelper mDbHelper;
    protected SQLiteDatabase mDb;

    public FollowActivity() {
        mDbHelper = new DbHelper(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent data = getIntent();

        //  get user basic data from previous activity....
        mUserId = data.getIntExtra("userId", 1);   // the profile target user id...
        mUserName = data.getStringExtra("userName");

        // the login user id
        mMyUserId = data.getIntExtra("myUserId", 2);

        // layout
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(0xcc9eb2c3));
        this.setContentView(R.layout.dialog_follow);

        findViewById(R.id.flo_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        name = (TextView) findViewById(R.id.flo_name);

        follower_number = (TextView) findViewById(R.id.flo_follower_number);
        following_number = (TextView) findViewById(R.id.flo_following_number);

        favourite_station = (TextView) findViewById(R.id.flo_station_favourite);
        favourite_line = (TextView) findViewById(R.id.flo_line_favourite);

        follow_button = (TextView) findViewById(R.id.flo_button_txt);
        // first  reset the follow button text...
        follow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickFollowButton(mMyUserId, mUserId);
            }
        });

        // then the name
        String myname = data.getStringExtra(DATA_KEY_NAME);
        if (myname == null || "".equalsIgnoreCase(myname.trim())) myname = "Koichi  Wada";
        name.setText(myname);

        // then the following. follower number...
        int following = data.getIntExtra(DATA_KEY_FOLLOWING_NUMBER, 0);
        following_number.setText(String.valueOf(following));
        int follower = data.getIntExtra(DATA_KEY_FOLLOWER_NUMBER, 0);
        follower_number.setText(String.valueOf(follower));

        // last the station and line
        String station = data.getStringExtra(DATA_KEY_FAV_STATION);
        if (station == null || "".equalsIgnoreCase(station.trim())) station = "Omote-sando";
        favourite_station.setText(station);

        String line = data.getStringExtra(DATA_KEY_FAV_LINE);
        if (line == null || "".equalsIgnoreCase(line.trim())) line = "Ginza";
        favourite_line.setText(line);

        // get profile detail from server...
        getProfile(mUserId);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDb = mDbHelper.getWritableDatabase();
        findViewById(R.id.follow_button).setVisibility(
                (mMyUserId == mUserId) ? View.GONE : View.VISIBLE
        );

        resetButtonTxt(isFollowingOnDb());
    }

    @Override
    protected void onPause() {
        if (mDb.isOpen()) {
            mDb.close();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void resetButtonTxt(boolean isFollowed) {
        if (isFollowed) {
            follow_button.setText("UnFollow");
        } else {
            follow_button.setText("Follow");
        }
    }


    JsonHttpResponseHandler profileCallback = new JsonHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            if (response == null || response.length() < 1) {
                return;
            }

            Log.d("Profile", "response: " + response.toString());
            try {
                String usename = response.getString("username");
                if (usename != null && !"".equalsIgnoreCase(usename.trim())) name.setText(usename);

                String following_count = response.getString("following_count");
                if (following_count != null && !"".equalsIgnoreCase(following_count.trim()))
                    following_number.setText(following_count);

                String follower_count = response.getString("follower_count");
                if (follower_count != null && !"".equalsIgnoreCase(follower_count.trim()))
                    follower_number.setText(follower_count);
                JSONObject aobj = response.getJSONArray("loc_info").getJSONObject(0);

                String fav_line = aobj.getString("odpt:railway");
                if (fav_line != null && !"".equalsIgnoreCase(fav_line.trim())) {
                    int offset = fav_line.indexOf("TokyoMetro.");
                    if (offset > 0) {
                        fav_line = fav_line.replaceFirst("odpt.Railway:TokyoMetro.", "");
                    }
                    favourite_line.setText(fav_line);
                }

                String fav_station = null;
                fav_station = aobj.getString("owl:sameAs");
                if (fav_station != null && !"".equalsIgnoreCase(fav_station.trim())) {
                    int offset = fav_station.indexOf("TokyoMetro.");
                    if (offset > 0) {
                        fav_station = fav_station.replaceFirst("odpt.Station:TokyoMetro.", "");
                        fav_station = fav_station.replaceFirst(fav_line + ".", "");
                    }
                    favourite_station.setText(fav_station);
                }

            } catch (JSONException e) {
                Log.e("Profile", " parseJsonObject failed with exception...", e);
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            super.onFailure(statusCode, headers, throwable, errorResponse);
        }

    };

    private void getProfile(int userId) {
        User.getProfile(userId, profileCallback);
    }


    AsyncHttpResponseHandler followCallback = new AsyncHttpResponseHandler() {
        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            Log.d("Profile", "response: " + responseBody.toString() + ":" + error.toString());

            Toast.makeText(FollowActivity.this, "Failed to Follow", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            // success
            followOnDb();
            resetButtonTxt(true);
            Toast.makeText(FollowActivity.this, "Followed", Toast.LENGTH_SHORT).show();

        }
    };

    AsyncHttpResponseHandler unfollowCallback = new AsyncHttpResponseHandler() {
        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            Log.d("Profile", "response: " + responseBody.toString() + ":" + error.toString());

            Toast.makeText(FollowActivity.this, "Failed to Unfollow", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            // success
            unfollowOnDb();
            resetButtonTxt(false);
            Toast.makeText(FollowActivity.this, "Unfollowed", Toast.LENGTH_SHORT).show();

        }
    };

    //  点击按钮。。。
    private void clickFollowButton(int myUserID, int targetUserID) {
        if (isFollowingOnDb()) {
            User.unfollow(mMyUserId, mUserId, unfollowCallback);
        } else {
            User.follow(mMyUserId, mUserId, followCallback);
        }
    }


    /*
     * Remenbering follow state
     */
    public boolean isFollowingOnDb() {
        Cursor cursor = mDb.rawQuery("SELECT follow_id FROM follow WHERE user_id = ? AND follow_id = ?",
                new String[]{Integer.valueOf(mMyUserId).toString(), Integer.valueOf(mUserId).toString()}
        );

        boolean ret = (cursor.getCount() == 1);

        Log.d("FOLLOW",
                new StringBuilder()
                        .append(mMyUserId)
                        .append(ret ? " follows " : " NOT follows ")
                        .append(mUserId)
                        .toString());

        return ret;
    }

    public void followOnDb() {
        if (mMyUserId != mUserId) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("user_id", mMyUserId);
            contentValues.put("follow_id", mUserId);

            mDb.insert("follow", null, contentValues);
        }
    }

    public void unfollowOnDb() {
        mDb.delete("follow", "user_id = ? AND follow_id = ?",
                new String[]{Integer.valueOf(mMyUserId).toString(), Integer.valueOf(mUserId).toString()}
        );
    }

}
