package info.sidespace.meetro;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.IntentSender;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationRequest;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import info.sidespace.meetro.api.Bitmap;
import info.sidespace.meetro.api.NearbyStation;

public class HomeActivity extends PostListActivityBase implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    private final Activity self = this;
    private GoogleMap mMap;
    private Marker mMarker;
    private ImageButton mPostButton;
    private HomeTabArea mTabArea;

    // for Google Location API
    private GoogleApiClient mApiClient;
    private LocationRequest mLocationRequest;
    private boolean mLocationUpdate = false;

    // Device location & Station
    private double mLat = 35.68159659;
    private double mLng = 139.76474047;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Intent intent = getIntent();
        mUserId = intent.getIntExtra("userId", 0);
        mUserName = intent.getStringExtra("userName");
        mStationName = getString(R.string.title_activity_home);

        Log.d("Log in:", new Integer(mUserId).toString());

        /*
         * Initialization for Bitmap API.
         */
        Bitmap.setup(this);

        /*
         * Map
         */
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        mMap.setMyLocationEnabled(true);
        mMap.setBuildingsEnabled(true);
        //mMap.setTrafficEnabled(true);
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(false);
        uiSettings.setMyLocationButtonEnabled(false);

        /*
         * Post button
         */
        mPostButton = (ImageButton) findViewById(R.id.postbutton);
        mPostButton.setVisibility(View.INVISIBLE);
        mPostButton.setOnClickListener(this);

        /*
         * TabArea
         */
        mTabArea = new HomeTabArea(this);
        mTabArea.setup();

        /*
         * Create a new api client for location
         */
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // set initial location
        setMapLocation(mStationName, mLat, mLng);

        Toast.makeText(self, "Welcome " + mUserName, Toast.LENGTH_SHORT).show();
    }

    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mTabArea.refreshPostList();

        if (mApiClient.isConnected()) {
            startLocationUpdate();
        } else if (mApiClient.isConnecting()) {
            // do nothing
        } else {
            // Not connected.
            mApiClient.connect();
        }
    }


    @Override
    protected void onPause() {
        stopLocationUpdate();

        super.onPause();
    }

    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        /*
         * After disconnect() is called, the client is
         * considered "dead".
         */
        mApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /*
     * GoogleApiClient.ConnectionCallbacks.onConnected()
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        Log.d("Event", "onConnected");
        // Display the connection status
        //Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        startLocationUpdate();
    }

    /*
     * GoogleApiClient.ConnectionCallbacks.onConnected()
     */
    @Override
    public void onConnectionSuspended(int i) {
        // Display the connection status
        //Toast.makeText(this, "Disconnected. Please re-connect.:" + new Integer(i).toString(),
        //        Toast.LENGTH_SHORT).show();
        stopLocationUpdate();
    }

    /*
     * GoogleApiClient.OnConnectionFailedListener.onConnectionFailed()
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    private void startLocationUpdate() {
        if (!mLocationUpdate && mApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mApiClient, mLocationRequest, this);
            mLocationUpdate = true;
        }
    }

    private void stopLocationUpdate() {
        if (mLocationUpdate && mApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mApiClient, this);
            mLocationUpdate = false;
        }
    }

    /**
     * LocationListener.onLocationChanged()
     *
     * @param location
     */
    public void onLocationChanged(Location location) {
        float[] distanceList = new float[]{0};

        try {
            Location.distanceBetween(mLat, mLng, location.getLatitude(), location.getLongitude(), distanceList);
        } catch (IllegalArgumentException e) {
            Log.d("onLocationChanged", "Bad distance", e);
            distanceList[0] = 0;
        }

        if (mUcode.equals("") || distanceList[0] >= 1) {
            updateMapLocation(location);
        }
    }

    /*
     * Check that Google Play services is available
     */
    private boolean isServicesConnected() {

        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        Log.d("ResultCode:", new Integer(resultCode).toString());
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            showErrorDialog(resultCode);
            // Get the error dialog from Google Play services
            return false;
        }
    }

    /*
      * Handle results returned to this Activity by other Activities started with
      * startActivityForResult(). In particular, the method onConnectionFailed() in
      * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
      * start an Activity that handles Google Play services problems. The result of this
      * call returns here, to onActivityResult.
      */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    /*
                     * Try the request again
                     */
                        Log.d("onActivityResult", "Resolved");
                        break;

                    default:
                        Log.d("onActivityResult", new StringBuilder("ResultCode:").append(requestCode).toString());
                        break;
                }

                break;

            default:
                Log.d("onActivityResult", new StringBuilder("RequestCode:").append(requestCode).toString());
                break;
        }
    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }

    }

    private void showErrorDialog(int resultCode) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                resultCode,
                this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {
            // Create a new DialogFragment for the error dialog
            ErrorDialogFragment errorFragment =
                    new ErrorDialogFragment();
            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);
            // Show the error dialog in the DialogFragment
            errorFragment.show(getFragmentManager(),
                    "Location Updates");
        }

    }


    // Location
    public void updateMapLocation(Location location) {
        final double lat = location.getLatitude();
        final double lng = location.getLongitude();

        NearbyStation.get(lat, lng, 700, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                if (response == null || response.length() == 0) {
                    mStationName = getString(R.string.no_stations_nearby);
                    setMapLocation(mStationName, lat, lng);
                    mUcode = "";
                } else {
                    try {
                        JSONObject stationInfo = response.getJSONObject(0);

                        String uCode = stationInfo.getString("@id");

                        if (!mUcode.equals(uCode)) {
                            Toast.makeText(self, getString(R.string.msg_found_nearby_station), Toast.LENGTH_SHORT).show();

                            mUcode = uCode;
                            Log.d("Station Code", mUcode);

                            String name = stationInfo.getString("owl:sameAs");
                            String[] namePartList = name.split("\\.");

                            StringBuilder titleBuilder
                                    = new StringBuilder(namePartList[namePartList.length - 1]);
                            titleBuilder.append('(').append(stationInfo.getString("odpt:stationCode"));

                            if (namePartList.length > 2) {
                                titleBuilder.append(':')
                                        .append(namePartList[namePartList.length - 2])
                                        .append(" Line");
                            }
                            titleBuilder.append(')');

                            mStationName = titleBuilder.toString();
                            setMapLocation(mStationName, stationInfo.getDouble("geo:lat"), stationInfo.getDouble("geo:long"));
                        }
                    } catch (JSONException e) {
                        Log.d("updateMapLocation", response.toString(), e);
                    }
                }

                mLat = lat;
                mLng = lng;
                mPostButton.setVisibility(mUcode.length() > 0 ? View.VISIBLE : View.INVISIBLE);
                mTabArea.refreshPostList();
                mTabArea.setEnabled(mUcode.length() > 0);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(self, getString(R.string.error_search_station), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setMapLocation(String title, double lat, double lng) {
        setTitle(title);
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        mMap.animateCamera(cameraUpdate);
        if (title.length() > 0 && !title.equals(getString(R.string.no_stations_nearby))) {
            if (mMarker == null) {
                mMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_maps_place)));
            } else {
                mMarker.setPosition(latLng);
                mMarker.setVisible(true);
            }
        } else {
            if (mMarker != null) {
                mMarker.setVisible(false);
            }
        }

    }

    /*
     * PostButton
     */
    public void onClick(View v) {
        openPostSubmit(0); // NO reply
    }
}