package info.sidespace.meetro.api;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by koichi-wada on 2014/11/05.
 */
public class NearbyStation {
    private static final String URL = "http://54.64.129.219//nearbyInfo.php";
    private static final String TYPE = "odpt:Station";

    public static void get(double lat, double lng, int radius, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("type", TYPE);
        params.put("lat", lat);
        params.put("lng", lng);
        params.put("radius", radius);
        Http.get(URL, params, responseHandler);
    }
}