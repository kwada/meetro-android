package info.sidespace.meetro.api;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by koichi-wada on 2014/11/06.
 */
public class Post {
    private static final String URL_POST_BY_UCODE = "http://54.64.129.219/postByUcode.php";
    private static final String URL_POST_SUBMIT = "http://54.64.129.219/postSubmit.php";
    private static final String URL_POST_LIKE = "http://54.64.129.219/postLike.php";
    private static final String URL_POST_UNLIKE = "http://54.64.129.219/postUnlike.php";

    public static final int TYPE_POPULAR = 0;
    public static final int TYPE_RECENT = 1;
    public static final int TYPE_TIPS = 2;
    public static final int TYPE_EVENT = 3;

    public static void getByUcode(int userId, int type, String ucode, int offset, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("ucode", ucode);
        params.put("current_user_id", userId);
        params.put("offset", offset);
        switch (type) {
            case TYPE_POPULAR:
                params.put("sort", "popularity");
                break;
            case TYPE_TIPS:
                params.put("category", "tips");
                break;
            case TYPE_EVENT:
                params.put("category", "event");
                break;
            default:
                break;
        }

        Http.get(URL_POST_BY_UCODE, params, responseHandler);
    }

    public static void submitPost(int userId, String ucode, String postData, String category,
                                  int parentPostId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);
        params.put("ucode", ucode);
        params.put("post_data", postData);
        if (category.length() > 0) {
            params.put("category", category);
        }
        if (parentPostId > 0) {
            params.put("parent_post_id", parentPostId);
        }

        Http.post(URL_POST_SUBMIT, params, responseHandler);
    }

    public static void like(int userId, int postId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);
        params.put("user_id", userId);
        params.put("post_id", postId);

        Http.post(URL_POST_LIKE, params, responseHandler);
    }

    public static void unlike(int userId, int postId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);
        params.put("user_id", userId);
        params.put("post_id", postId);

        Http.post(URL_POST_UNLIKE, params, responseHandler);
    }
}
