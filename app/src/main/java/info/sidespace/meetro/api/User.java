package info.sidespace.meetro.api;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by wada on 14/11/14.
 */
public class User {
    private static String URL_GET_PROFILE = "http://54.64.129.219/userInfo.php";
    private static String URL_FOLLOW = "http://54.64.129.219/userFollow.php";
    private static String URL_UNFOLLOW = "http://54.64.129.219/userUnfollow.php";

    public static void getProfile(int userId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);

        Http.get(URL_GET_PROFILE, params, responseHandler);
    }

    public static void follow(int userId, int followerId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);
        params.put("follower_id", followerId);

        Http.post(URL_FOLLOW, params, responseHandler);
    }

    public static void unfollow(int userId, int followerId, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("user_id", userId);
        params.put("follower_id", followerId);

        Http.post(URL_UNFOLLOW, params, responseHandler);
    }
}
