package info.sidespace.meetro.api;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by wada on 14/11/09.
 */
public class Login {
    private static final String URL = "http://54.64.129.219/login.php";

    public static void login(String userName, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams("username", userName);

        Http.post(URL, params, responseHandler);
    }
}
