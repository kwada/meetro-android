package info.sidespace.meetro;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import info.sidespace.meetro.api.Bitmap;
import info.sidespace.meetro.api.User;

/**
 * Created by wada on 14/11/14.
 */
public class ProfileDialogFragment extends DialogFragment {
    private PostListActivityBase mActivity;
    private PostArrayAdapter mAdapter;
    private int mUserId;
    private int mFollowId;


    public static ProfileDialogFragment newInstance(PostListActivityBase activity, PostArrayAdapter adapter, int userId, int followId) {
        activity.setPostArrayAdapter(adapter);

        ProfileDialogFragment fragment = new ProfileDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId);
        bundle.putInt("followId", followId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle bundle) {
        Log.d("DIALOG EVENT", "onCreateDialog");
        mActivity = (PostListActivityBase) getActivity();
        mAdapter = mActivity.getPostArrayAdapter();
        mUserId = getArguments().getInt("userId");
        mFollowId = getArguments().getInt("followId");

        Dialog dialog = new Dialog(getActivity());
        // No title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Full screen
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.setContentView(R.layout.dialog_follow);
        // Transparent background
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ImageView userIconView = (ImageView) dialog.findViewById(R.id.flo_head);
        Bitmap.get(mFollowId, new Bitmap.ResponseHandler() {
            @Override
            public void onResponse(android.graphics.Bitmap bitmap) {
                if (bitmap != null) {
                    userIconView.setImageBitmap(bitmap);
                }
            }
        });

        if (mUserId == mFollowId) {
            dialog.findViewById(R.id.follow_button).setVisibility(View.GONE);
        } else {
            ((TextView) dialog.findViewById(R.id.flo_button_txt)).setText(
                    isFollowing() ? "Unfollow" : "Follow"
            );

            // follow button
            dialog.findViewById(R.id.flo_button_txt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isFollowing()) {
                        unfollow();
                    } else {
                        follow();
                    }
                }
            });
        }

        // close button
        dialog.findViewById(R.id.flo_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        getProfile();

        return dialog;
    }

    public void getProfile() {
        User.getProfile(mFollowId, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response == null || response.length() < 1) {
                    return;
                }

                Log.d("Profile", "response: " + response.toString());
                Dialog dialog = getDialog();
                try {
                    String usename = response.getString("username");
                    if (usename != null && !"".equalsIgnoreCase(usename.trim())) {
                        ((TextView) dialog.findViewById(R.id.flo_name)).setText(usename);
                    }

                    String following_count = response.getString("following_count");
                    if (following_count != null && !"".equalsIgnoreCase(following_count.trim())) {
                        ((TextView) dialog.findViewById(R.id.flo_following_number)).setText(following_count);
                    }

                    String follower_count = response.getString("follower_count");
                    if (follower_count != null && !"".equalsIgnoreCase(follower_count.trim())) {
                        ((TextView) dialog.findViewById(R.id.flo_follower_number)).setText(follower_count);
                    }

                    JSONObject aobj = response.getJSONArray("loc_info").getJSONObject(0);
                    String fav_line = aobj.getString("odpt:railway");
                    if (fav_line != null && !"".equalsIgnoreCase(fav_line.trim())) {
                        int offset = fav_line.indexOf("TokyoMetro.");
                        if (offset > 0) {
                            fav_line = fav_line.replaceFirst("odpt.Railway:TokyoMetro.", "");
                        }

                        ((TextView) dialog.findViewById(R.id.flo_line_favourite)).setText(fav_line);
                    }

                    String fav_station = null;
                    fav_station = aobj.getString("owl:sameAs");
                    if (fav_station != null && !"".equalsIgnoreCase(fav_station.trim())) {
                        int offset = fav_station.indexOf("TokyoMetro.");
                        if (offset > 0) {
                            fav_station = fav_station.replaceFirst("odpt.Station:TokyoMetro.", "");
                            fav_station = fav_station.replaceFirst(fav_line + ".", "");
                        }
                        ((TextView) dialog.findViewById(R.id.flo_station_favourite)).setText(fav_station);
                    }

                } catch (JSONException e) {
                    Log.e("Profile", " parseJsonObject failed with exception...", e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    public boolean isFollowing() {
        return mActivity.isFollowing(mFollowId);
    }

    public void follow() {
        if (mUserId == mFollowId) return;

        User.follow(mUserId, mFollowId, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // success
                mActivity.follow(mFollowId);
                ((TextView) getDialog().findViewById(R.id.flo_button_txt)).setText("Unfollow");
                try {
                    int count = Integer.parseInt(((TextView) getDialog().findViewById(R.id.flo_following_number))
                            .getText().toString());
                    ++count;
                    ((TextView) getDialog().findViewById(R.id.flo_following_number)).setText(Integer.valueOf(count).toString());
                } catch (NumberFormatException e) {
                    Log.d("Exception in follow", e.toString());
                }

                mAdapter.notifyDataSetChanged();
                Toast.makeText(mActivity, "Followed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("Profile", "response: " + responseBody.toString() + ":" + error.toString());

                Toast.makeText(mActivity, "Failed to Follow", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void unfollow() {
        if (mUserId == mFollowId) return;

        User.unfollow(mUserId, mFollowId, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                mActivity.unfollow(mFollowId);
                ((TextView) getDialog().findViewById(R.id.flo_button_txt)).setText("Follow");
                try {
                    int count = Integer.parseInt(((TextView) getDialog().findViewById(R.id.flo_following_number))
                            .getText().toString());
                    if (count > 0) {
                        --count;
                    }
                    ((TextView) getDialog().findViewById(R.id.flo_following_number)).setText(Integer.valueOf(count).toString());
                } catch (NumberFormatException e) {
                    Log.d("Exception in follow", e.toString());
                }

                mAdapter.notifyDataSetChanged();
                Toast.makeText(mActivity, "Unfollowed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("Profile", "response: " + responseBody.toString() + ":" + error.toString());

                Toast.makeText(mActivity, "Failed to Unfollow", Toast.LENGTH_SHORT).show();
            }
        });
    }
}