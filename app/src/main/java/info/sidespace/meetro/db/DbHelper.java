package info.sidespace.meetro.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;

/**
 * Created by wada on 14/11/10.
 */
public class DbHelper extends SQLiteOpenHelper {
    private final static String DB_NAME = "meetro";
    private final static int DB_VERSION = 3;
    private final static String SQL_CREATE_FOLLOW
            = "CREATE TABLE IF NOT EXISTS follow (user_id integer, follow_id interger, PRIMARY KEY(user_id, follow_id))";
    private final static String SQL_DROP_LIKED = "DROP TABLE IF EXISTS liked";
    private final static String SQL_DROP_FOLLOWED = "DROP TABLE IF EXISTS followed";
    private final static String SQL_DROP_FOLLOW = "DROP TABLE IF EXISTS follow";

    private Context mContext;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FOLLOW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_LIKED);
        db.execSQL(SQL_DROP_FOLLOWED);
        db.execSQL(SQL_DROP_FOLLOW);
        onCreate(db);
    }
}
