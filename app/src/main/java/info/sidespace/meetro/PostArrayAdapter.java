package info.sidespace.meetro;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;


import java.util.List;

import info.sidespace.meetro.api.Bitmap;
import info.sidespace.meetro.api.Post;
import info.sidespace.meetro.model.PostData;

/**
 * Created by wada on 14/11/13.
 */
public class PostArrayAdapter extends ArrayAdapter<PostData> {
    private static final int USER_TOKYO_METRO = 2;

    private PostArrayAdapter mSelf;
    private PostListActivityBase mActivity;
    private boolean mOpenConversation;
    private LayoutInflater mInflater;

    // Color filter
    private LightingColorFilter mLikedFilter = new LightingColorFilter(Color.LTGRAY, Color.BLUE);

    public PostArrayAdapter(PostListActivityBase activity, boolean openConversation, List<PostData> data) {
        super(activity, R.layout.listitem_post, data);

        mSelf = this;
        mActivity = activity;
        mOpenConversation = openConversation;
        mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final PostData postData = (PostData) getItem(position);
        Log.d("PostData:UserId", Integer.valueOf(postData.mUserId).toString());
        Log.d("PostData:PostData", postData.mPostData);

        if (null == convertView) {
            convertView = mInflater.inflate(R.layout.listitem_post, null);
        }

        // followed/unfollowed
        convertView.setBackgroundResource(
                mActivity.isFollowing(postData.mUserId)
                        ? R.color.followed
                        : R.color.unfollowed
        );

        // opening conversation
        if (mOpenConversation) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (postData.mParentPostId > 0 || postData.mReplyPostIdList.length > 0) {
                        mActivity.openConversation(postData.mId);
                    }
                }
            });
        }

        // post data
        TextView textView = (TextView) convertView.findViewById(R.id.Post);
        textView.setText(postData.mPostData);
        ((TextView) convertView.findViewById(R.id.timestamp)).setText(postData.mTimestamp);

        // user icon
        final ImageButton userIcon = (ImageButton) convertView.findViewById(R.id.userIcon);
        userIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    DialogFragment dialogFragment = ProfileDialogFragment.newInstance(
                            mActivity,
                            mSelf,
                            mActivity.getUserId(),
                            postData.mUserId
                    );

                    dialogFragment.show(mActivity.getFragmentManager(), "Profile");

                    Log.d("USER PROFILE", Integer.valueOf(postData.mUserId).toString());
                } catch (NumberFormatException e) {
                    Log.d("ERROR SHOW PROILE", e.toString());
                }
            }
        });

        Log.d("**PostData:UserId", Integer.valueOf(postData.mUserId).toString());
        userIcon.setImageResource(R.drawable.ic_content_anonymous);

        Bitmap.get(postData.mUserId, new Bitmap.ResponseHandler() {
            @Override
            public void onResponse(android.graphics.Bitmap bitmap) {
                if (bitmap != null) {
                    userIcon.setImageBitmap(bitmap);
                }
            }
        });

        // no reply button, etc. for Tokyo Metro account.
        if (postData.mUserId == USER_TOKYO_METRO) {
            convertView.findViewById(R.id.toolArea).setVisibility(View.GONE);
            return convertView;
        }

        convertView.findViewById(R.id.toolArea).setVisibility(View.VISIBLE);

        ImageButton replyButton = (ImageButton) convertView.findViewById(R.id.replyButton);
        replyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int inReplyTo = postData.mId;
                    Log.d("REPLY TO", Integer.valueOf(inReplyTo).toString());
                    mActivity.openPostSubmit(inReplyTo);
                } catch (NumberFormatException e) {
                    Log.d("ERROR REPLY TO", e.toString());
                }
            }
        });

        // reply label
        String replyLabel = (postData.mReplyCount > 0)
                ? Integer.valueOf(postData.mReplyCount).toString()
                : mActivity.getString(R.string.post_label_reply);

        ((TextView) convertView.findViewById(R.id.replyLabel)).setText(replyLabel);


        ImageButton likeButton = (ImageButton) convertView.findViewById(R.id.likeButton);
        if (postData.isLiked()) {
            likeButton.setColorFilter(mLikedFilter);
        } else {
            likeButton.clearColorFilter();
        }

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (postData.isLiked()) {
                        Post.unlike(mActivity.getUserId(), postData.mId, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.d("UNLIKED", responseBody.toString());
                                postData.unlike();
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.d("FAILED TO LIKE", responseBody.toString());
                            }
                        });
                    } else {
                        Post.like(mActivity.getUserId(), postData.mId, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.d("LIKED", responseBody.toString());
                                postData.like();
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.d("FAILED TO LIKE", responseBody.toString());
                            }
                        });
                    }
                } catch (NumberFormatException e) {
                    Log.d("ERROR LIKE", e.toString());
                }
            }
        });

        // like label
        String likeLabel = (postData.mLikeCount > 0)
                ? Integer.valueOf(postData.mLikeCount).toString()
                : mActivity.getString(R.string.post_label_like);

        ((TextView) convertView.findViewById(R.id.likeLabel)).setText(likeLabel);


        return convertView;
    }
}

