package info.sidespace.meetro;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import info.sidespace.meetro.api.Post;


public class PostSubmitActivity extends Activity {

    private Activity mSelf;
    private int mUserId;
    private String mUserName;
    private String mUcode;
    private String mStationName;
    private int mInReplyTo;

    private EditText mPostText;

    private CheckBox mTipsCheckBox;
    private CheckBox mEventCheckBox;

    private Button mCancelButton;
    private Button mSubmitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_submit);

        Intent intent = getIntent();
        mUserId = intent.getIntExtra("userId", 0);
        mUserName = intent.getStringExtra("userName");
        mUcode = intent.getStringExtra("ucode");
        mStationName = intent.getStringExtra("stationName");
        mInReplyTo = intent.getIntExtra("inReplyTo", 0);

        mSelf = this;

        /*
         * Title
         */
        if (mInReplyTo > 0) {
            setTitle(getString(R.string.title_activity_post_submit_reply));
        }

        /*
         * Post text
         */
        mPostText = (EditText) findViewById(R.id.postText);
        if (mInReplyTo > 0) {
            mPostText.setHint(getString(R.string.post_hint_reply));
        } else {
            mPostText.setHint(getString(R.string.post_hint));
        }

        /*
         * Station name
         */
        ((TextView) findViewById(R.id.stationName)).setText("@" + mStationName);

        /*
         * Category checkbox
         */
        mTipsCheckBox = (CheckBox) findViewById(R.id.tips);
        mEventCheckBox = (CheckBox) findViewById(R.id.event);
        mTipsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mEventCheckBox.setChecked(false);
                }
            }
        });

        mEventCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mTipsCheckBox.setChecked(false);
                }
            }
        });

        /*
         * Post button
         */
        mCancelButton = (Button) findViewById(R.id.cancelButton);
        mSubmitButton = (Button) findViewById(R.id.submitButton);

        if (mInReplyTo > 0) {
            mSubmitButton.setText(getString(R.string.button_label_reply));
        }

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

    public void cancel() {
        enableEdit(false);
        finish();
    }

    public void submit() {
        enableEdit(false);

        final ProgressDialog waitDialog = new ProgressDialog(this);
        waitDialog.setMessage(getString(R.string.msg_post_submitting));
        waitDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        waitDialog.show();

        String postText = mPostText.getText().toString().trim();
        if (postText.length() == 0) {
            enableEdit(true);
        }

        String category = "";
        if (mTipsCheckBox.isChecked()) {
            category = getString(R.string.category_tips);
        } else if (mEventCheckBox.isChecked()) {
            category = getString(R.string.category_event);
        }

        Post.submitPost(mUserId, mUcode, postText, category, mInReplyTo, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d("SUBMIT SUCCESS", responseBody.toString());

                Toast.makeText(mSelf, getString(R.string.msg_post_submitted), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("SUBMIT FAIL", responseBody.toString());

                Toast.makeText(mSelf, getString(R.string.msg_error_post_submit), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                waitDialog.dismiss();
                enableEdit(true);
            }
        });
    }

    public void enableEdit(boolean enable) {
        mPostText.setEnabled(enable);
        mTipsCheckBox.setEnabled(enable);
        mEventCheckBox.setEnabled(enable);
        mCancelButton.setEnabled(enable);
        mSubmitButton.setEnabled(enable);
    }

}
