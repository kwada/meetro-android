package info.sidespace.meetro;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import info.sidespace.meetro.api.Post;
import info.sidespace.meetro.model.PostData;

/**
 * Created by koichi-wada on 2014/11/07.
 */
public class HomeTabArea {

    private HomeActivity mActivity;
    private TabHost mTabHost;
    private Tab[] mTabList;

    public HomeTabArea(HomeActivity activity) {
        mActivity = activity;
        mTabHost = (TabHost) mActivity.findViewById(R.id.tabhost);
        mTabList = new Tab[4];
    }

    public void setup() {
        mTabHost.setup();

        mTabList[0] = new Tab("tab1", R.id.tab1, R.id.list1,
                mActivity.getString(R.string.tab_popular), Post.TYPE_POPULAR);
        mTabList[1] = new Tab("tab2", R.id.tab2, R.id.list2,
                mActivity.getString(R.string.tab_recent), Post.TYPE_RECENT);
        mTabList[2] = new Tab("tab3", R.id.tab3, R.id.list3,
                mActivity.getString(R.string.tab_tips), Post.TYPE_TIPS);
        mTabList[3] = new Tab("tab4", R.id.tab4, R.id.list4,
                mActivity.getString(R.string.tab_event), Post.TYPE_EVENT);

        for (Tab tab : mTabList) {
            mTabHost.addTab(tab.newTabSpec());
        }

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                refreshPostList();
            }
        });
    }

    public void setEnabled(boolean enable) {
        mTabHost.setEnabled(enable);
        mTabHost.getTabWidget().setEnabled(enable);
        mTabHost.getTabContentView().setEnabled(enable);

        for (Tab tab : mTabList) {
            tab.setEnabled(enable);
        }
    }

    public void refreshPostList() {
        Log.d("CurrentTab", Integer.valueOf(mTabHost.getCurrentTab()).toString());
        Tab tab = mTabList[mTabHost.getCurrentTab()];
        tab.refresh();
    }

    public class Tab {
        private String mTag;
        private int mViewId;
        private int mListId;
        private String mLabel;
        private int mPostType;
        private SwipeRefreshLayout mSwipeRefreshLayout;
        private ListView mListView;
        private JsonHttpResponseHandler mPostRefreshHandler;

        public Tab(String tag, int viewId, int listId, String label, int postType) {
            mTag = tag;
            mViewId = viewId;
            mListId = listId;
            mLabel = label;
            mPostType = postType;
            mSwipeRefreshLayout = (SwipeRefreshLayout) mActivity.findViewById(mViewId);
            mListView = (ListView) mActivity.findViewById(mListId);

            PostArrayAdapter adapter = new PostArrayAdapter(mActivity, true, new ArrayList<PostData>());
            mListView.setAdapter(adapter);

            mPostRefreshHandler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("Refresh Post:", response.toString());
                    try {
                        List<PostData> postList = new ArrayList<PostData>();
                        for (int i = 0; i < response.length(); ++i) {
                            PostData postData = new PostData(response.getJSONObject(i));
                            Log.d("POST DATA", postData.mPostData);
                            Log.d("POST ID", Integer.valueOf(postData.mId).toString());
                            postList.add(postData);
                        }

                        PostArrayAdapter adapter = (PostArrayAdapter) mListView.getAdapter();
                        adapter.clear();
                        adapter.addAll(postList);
                    } catch (JSONException e) {
                        Log.d("Exception", e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("Error", responseString);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            };

            mSwipeRefreshLayout.setColorSchemeResources(
                    R.color.blue, R.color.green, R.color.orange, R.color.red);

            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refresh();
                        }
                    }, 500);


                }
            });
        }

        public TabHost.TabSpec newTabSpec() {
            return mTabHost.newTabSpec(mTag).setContent(mViewId).setIndicator(mLabel);
        }

        public void setEnabled(boolean enable) {
            mSwipeRefreshLayout.setEnabled(enable);
        }

        public void refresh() {
            String ucode = mActivity.getUcode();
            if (ucode.length() < 1) {
                ((ArrayAdapter) mListView.getAdapter()).clear();
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            } else {
                Post.getByUcode(mActivity.getUserId(), mPostType, ucode, 0, mPostRefreshHandler);
            }
        }
    }
}
