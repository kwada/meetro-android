package info.sidespace.meetro;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import info.sidespace.meetro.api.User;
import info.sidespace.meetro.db.DbHelper;

/**
 * Created by wada on 14/11/13.
 */
public class PostListActivityBase extends Activity {
    protected int mUserId = 0;
    protected String mUserName = "";
    protected String mUcode = "";
    protected String mStationName = "";

    // DB
    private DbHelper mDbHelper;
    protected SQLiteDatabase mDb;

    // Profile dialog support
    private PostArrayAdapter mPostArrayAdapter;

    public PostListActivityBase() {
        mDbHelper = new DbHelper(this);
    }

    public int getUserId() {
        return mUserId;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getUcode() {
        return mUcode;
    }

    public String getStationName() {
        return mStationName;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDb = mDbHelper.getWritableDatabase();
    }

    @Override
    protected void onPause() {
        if (mDb.isOpen()) {
            mDb.close();
        }

        super.onPause();
    }

    /*
     * Opens post submit page
     */
    public void openPostSubmit(int inReplyTo) {
        Intent intent = new Intent(this, PostSubmitActivity.class);
        intent.putExtra("userId", mUserId);
        intent.putExtra("userName", mUserName);
        intent.putExtra("ucode", mUcode);
        intent.putExtra("stationName", mStationName);
        intent.putExtra("inReplyTo", inReplyTo);
        startActivity(intent);
    }

    /*
   * Follow
   */
    public boolean isFollowing(int followId) {
        Cursor cursor = mDb.rawQuery("SELECT follow_id FROM follow WHERE user_id = ? AND follow_id = ?",
                new String[]{
                        new Integer(mUserId).toString(),
                        new Integer(followId).toString()
                });

        boolean ret = (cursor.getCount() == 1);

        Log.d("FOLLOW",
                new StringBuilder()
                        .append(mUserId)
                        .append(ret ? " follows " : " NOT follows ")
                        .append(followId)
                        .toString());

        return ret;
    }


    public void follow(int followId) {
        if (mUserId != followId) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("user_id", mUserId);
            contentValues.put("follow_id", followId);

            mDb.insert("follow", null, contentValues);
        }
    }

    /*
     * unfollow
     */
    public void unfollow(int followId) {
        mDb.delete("follow", "user_id = ? AND follow_id = ?",
                new String[]{
                        new Integer(mUserId).toString(),
                        new Integer(followId).toString()
                }
        );
    }


    /*
     * Opens conversation
     */
    public void openConversation(int postId) {
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra("postId", postId);
        intent.putExtra("userId", mUserId);
        intent.putExtra("userName", mUserName);
        intent.putExtra("ucode", mUcode);
        intent.putExtra("stationName", mStationName);
        startActivity(intent);
    }


    /*
     * Profile dialog support
     */
    public void setPostArrayAdapter(PostArrayAdapter adapter) {
        mPostArrayAdapter = adapter;
    }

    public PostArrayAdapter getPostArrayAdapter() {
        return mPostArrayAdapter;
    }
}
