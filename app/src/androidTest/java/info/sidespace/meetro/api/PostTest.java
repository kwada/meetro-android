package info.sidespace.meetro.api;

import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by koichi-wada on 2014/11/06.
 */
public class PostTest extends InstrumentationTestCase {

    protected boolean mDone;
    protected JSONArray mJsonArray;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mDone = false;
        mJsonArray = null;
    }

    public void testGetRecentByUcode() throws Throwable {

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                Post.getByUcode(Post.TYPE_RECENT, "urn:ucode:_00001C000000000000010000030C46D4", 0,
                        new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                super.onSuccess(statusCode, headers, response);
                                mJsonArray = response;
                            }

                            @Override
                            public void onFinish() {
                                super.onFinish();
                                mDone = true;
                            }
                        });
            }
        });

        synchronized (this) {
            this.wait(3000);
        }

        assertTrue(mDone);
        assertTrue(mJsonArray.length() > 0);

        JSONObject o = mJsonArray.getJSONObject(0);
        assertNotNull(o);
        assertNotNull(o.getString("timestamp"));
        assertNotNull(o.getString("category"));
        assertNotNull(o.getString("name_en"));
        assertNotNull(o.getDouble("lat"));
        assertNotNull(o.getDouble("lng"));
        assertNotNull(o.getInt("user_id"));
        assertNotNull(o.getString("name_jp"));
        assertNotNull(o.getString("post_data"));
        assertNotNull(o.getString("loc_ucode"));
        assertNotNull(o.getInt("post_id"));
        assertNotNull(o.getString("ucode"));
    }
}
